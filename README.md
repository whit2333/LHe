# LHe

To simulate the optical photons inside the He-4 container originated from the hadrons.

## How to build

```
cmake -B build -S .
cmake --build -j4
```

## How to generate macro files
```
./generator.py
```

will generate the macro files that simulates 1000 events. Each event contains a bunch of optical photons and hits that the optical photons hit the nanowire. The x, y, z, t hit information will be saved in the TTree hits. The spatial origin of primary is (0, 0, 0) with time 0.

## How to run macro files

```
./build/LHe macros/().mac
```

should save root files in the output/.

The output file contains the TTree of hits_x, hits_y, hits_z.

We recommend to run the program at one of the computing farms.
ex) /volatile/clas12/sangbaek/LHe/output/.

## How to analyze the output

We recommend to use the uproot and python for those familiar with python.

```
python3 -m pip install uproot awkward-pandas
jupyter notebook Postprocessing.ipynb
```
For those familiar with root, please find the TTree `hits` that contains the vector<double> `hits_x`, `hits_y`, `hits_z`, and `hits_t`. Currently, the macro files are simulating 1000 events for each set-up.