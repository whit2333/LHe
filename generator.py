#!/usr/bin/env python3
import argparse
import numpy as np

def main():
  # parser = argparse.ArgumentParser(description="Get args",formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  # parser.add_argument("-E", "--pEnergy", type = int, default = 300)
  # parser.add_argument("-id", "--pType", type = str, default = "p")

  # args = parser.parse_args()

  # pType   = args.pType
  # pEnergy = args.pEnergy

  pTypes    = ["p", "D", "T", "He3", "He4"]
  pEnergies = [20, 40, 60, 80, 100]#in MeV
  phis      = [0, 10, 20, 30, 40, 50, 60]
  thetas    = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]

  for pType in pTypes:
    for pEnergy in pEnergies: 
      if pType == "p":
        Z, A, Q = 1, 1, 1
      elif pType == "D":
        Z, A, Q = 1, 2, 1
      elif pType == "T":
        Z, A, Q = 1, 3, 1
      elif pType == "He3":
        Z, A, Q = 2, 3, 2
      elif pType == "He4":
        Z, A, Q = 2, 4, 2
      else:
        return

      for theta in thetas:
        for phi in phis:
          filename = "{}_{}MeV_{}deg_{}deg".format(pType, pEnergy, theta, phi)
          output = "/run/initialize\n\n"
          output += "/analysis/setFileName output/{}\n\n".format(filename)
          output += "/gun/energy {} MeV\n".format(pEnergy)
          dirX = np.cos(np.radians(phi)) * np.sin(np.radians(theta))
          dirY = np.sin(np.radians(phi)) * np.sin(np.radians(theta))
          dirZ = np.cos(np.radians(theta))
          output += "/gun/direction {:.4f} {:.4f} {:.4f}\n\n".format(dirX, dirY, dirZ)
          output += "/gun/particle ion\n"
          output += "/gun/ion {} {} {} \n\n".format(Z, A, Q)

          output += "/run/beamOn 1000"

          with open("macros/{}.mac".format(filename), "w") as ofile:
            ofile.write(output)
          if theta == 0:
            break

if __name__ == '__main__':
  main()
