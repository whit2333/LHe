//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/src/LHeNanowireSD.cc
/// \brief Implementation of the LHeNanowireSD class
//
//
#include "LHeNanowireSD.hh"

#include "LHeDetectorConstruction.hh"
#include "LHeNanowireHit.hh"
#include "LHeUserTrackInformation.hh"

#include "G4ios.hh"
#include "G4LogicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4SDManager.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "G4Track.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VTouchable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeNanowireSD::LHeNanowireSD(G4String name)
  : G4VSensitiveDetector(name)
  , fNanowireHitCollection(nullptr)
  , fNanowirePositionsX(nullptr)
  , fNanowirePositionsY(nullptr)
  , fNanowirePositionsZ(nullptr)
  , fHitCID(-1)
{
  collectionName.insert("nanowireHitCollection");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeNanowireSD::~LHeNanowireSD()
{
  delete fNanowirePositionsX;
  delete fNanowirePositionsY;
  delete fNanowirePositionsZ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeNanowireSD::SetNanowirePositions(const std::vector<G4ThreeVector>& positions)
{
  for(size_t i = 0; i < positions.size(); ++i)
  {
    if(fNanowirePositionsX)
      fNanowirePositionsX->push_back(positions[i].x());
    if(fNanowirePositionsY)
      fNanowirePositionsY->push_back(positions[i].y());
    if(fNanowirePositionsZ)
      fNanowirePositionsZ->push_back(positions[i].z());
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeNanowireSD::Initialize(G4HCofThisEvent* hitsCE)
{
  fNanowireHitCollection =
    new LHeNanowireHitsCollection(SensitiveDetectorName, collectionName[0]);

  if(fHitCID < 0)
  {
    fHitCID = G4SDManager::GetSDMpointer()->GetCollectionID(fNanowireHitCollection);
  }
  hitsCE->AddHitsCollection(fHitCID, fNanowireHitCollection);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool LHeNanowireSD::ProcessHits(G4Step*, G4TouchableHistory*) { return false; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// Generates a hit and uses the postStepPoint's mother volume replica number
// PostStepPoint because the hit is generated manually when the photon is
// absorbed by the photocathode

G4bool LHeNanowireSD::ProcessHits_boundary(const G4Step* aStep, G4TouchableHistory* touchable)
{
  // need to know if this is an optical photon
  if(aStep->GetTrack()->GetDefinition() !=
     G4OpticalPhoton::OpticalPhotonDefinition())
    return false;

  if (!(aStep->IsFirstStepInVolume()))
    return false;
  // User replica number 1 since nanowire is a daughter volume
  // to the substrateboard which was replicated
  G4int nanowireNumber =
    aStep->GetPostStepPoint()->GetTouchable()->GetReplicaNumber(1);
  G4VPhysicalVolume* physVol =
    aStep->GetPostStepPoint()->GetTouchable()->GetVolume(1);

  // Find the correct hit collection
  size_t n       = fNanowireHitCollection->entries();
  LHeNanowireHit* hit = nullptr;
  for(size_t i = 0; i < n; ++i)
  {
    if((*fNanowireHitCollection)[i]->GetNanowireNumber() == nanowireNumber)
    {
      hit = (*fNanowireHitCollection)[i];
      break;
    }
  }

  if(hit == nullptr)
  {                         // this nanowire wasn't previously hit in this event
    hit = new LHeNanowireHit();  // so create new hit
    hit->SetNanowireNumber(nanowireNumber);
    hit->SetNanowirePhysVol(physVol);
    fNanowireHitCollection->insert(hit);
    hit->SetNanowirePos((*fNanowirePositionsX)[nanowireNumber], (*fNanowirePositionsY)[nanowireNumber],
                   (*fNanowirePositionsZ)[nanowireNumber]);
  }
  G4ThreeVector pos = (aStep->GetPostStepPoint()->GetPosition()) / CLHEP::cm;
  auto          transform     = touchable->GetHistory()->GetTopTransform();
  G4ThreeVector localPosition = transform.TransformPoint(worldPosition);

  G4double time = (aStep->GetPostStepPoint()->GetGlobalTime()) / CLHEP::ns; // in ns
  hit->SetNanowireHitPos(pos);
  hit->SetNanowireHitTOA(time);

  hit->IncPhotonCount();  // increment hit for the selected nanowire
  hit->SetDrawit(true);

  return true;
}
