//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/src/LHeDetectorConstruction.cc
/// \brief Implementation of the LHeDetectorConstruction class
//
//
#include "LHeDetectorConstruction.hh"

#include "LHeDetectorMessenger.hh"
#include "LHeMainVolume.hh"
#include "LHeNanowireSD.hh"
#include "LHeScintSD.hh"

#include "globals.hh"
#include "G4Box.hh"
#include "G4GeometryManager.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4LogicalVolume.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4OpticalSurface.hh"
#include "G4PhysicalConstants.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4PVPlacement.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4SolidStore.hh"
#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"
#include "G4Tubs.hh"
#include "G4UImanager.hh"
#include "G4VisAttributes.hh"

LHeDetectorConstruction::LHeDetectorConstruction()
  : fLHe_mt(nullptr)
{
  fExperimentalHall_tubs  = nullptr;
  fExperimentalHall_log  = nullptr;
  fExperimentalHall_phys = nullptr;

  fLHe = fAl = fAir = fVacuum = nullptr;
  fHe = fD = fN = fO = fC = fH = nullptr;

  fSaveThreshold = 0;
  SetDefaults();

  DefineMaterials();
  fDetectorMessenger = new LHeDetectorMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeDetectorConstruction::~LHeDetectorConstruction()
{
  if(fMainVolume)
  {
    delete fMainVolume;
  }
  delete fLHe_mt;
  delete fDetectorMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::DefineMaterials()
{
  G4double a;  // atomic mass
  G4double z;  // atomic number
  G4double density;

  //***Elements
  fH = new G4Element("H", "H", z = 1., a = 1.01 * g / mole);
  fD = new G4Element("D", "D", z = 1., a = 2.01 * g / mole);
  fHe = new G4Element("He", "He", z = 1., a = 4.00 * g / mole);
  fC = new G4Element("C", "C", z = 6., a = 12.01 * g / mole);
  fN = new G4Element("N", "N", z = 7., a = 14.01 * g / mole);
  fO = new G4Element("O", "O", z = 8., a = 16.00 * g / mole);

  //***Materials
  // Liquid Helium
  fLHe = new G4Material("LHe", z = 2., a = 4.002602 * g / mole,
                        density = 0.1248 * g / cm3);
  // Aluminum
  fAl = new G4Material("Al", z = 13., a = 26.98 * g / mole,
                       density = 2.7 * g / cm3);
  // Vacuum
  fVacuum = new G4Material("Vacuum", z = 1., a = 1.01 * g / mole,
                           density = universe_mean_density, kStateGas,
                           0.1 * kelvin, 1.e-19 * pascal);
  // Air
  fAir = new G4Material("Air", density = 1.29 * mg / cm3, 2);
  fAir->AddElement(fN, .7);
  fAir->AddElement(fO, .3);

  // ND3 (https://github.com/gemc/clas12Tags/blob/main/experiments/clas12/targets/target__materials_ND3.txt)
  fND3 = new G4Material("ND3", density = 0.6622 * mg / cm3, 3);
  fND3->AddElement(fN, .912*.25);
  fND3->AddElement(fD, .912*.75);
  fND3->AddElement(fHe, 1 - .912);

  //***Material properties tables
  //https://journals.aps.org/pra/abstract/10.1103/PhysRevA.5.372
  std::vector<G4double> lhe_Energy = {11.90 * eV, 11.94 * eV, 11.98 * eV, 12.02 * eV, 12.06 * eV,
   12.10 * eV, 12.14 * eV, 12.18 * eV, 12.22 * eV, 12.26 * eV, 12.30 * eV, 12.34 * eV, 12.38 * eV,
   12.42 * eV, 12.46 * eV, 12.50 * eV, 12.54 * eV, 12.58 * eV, 12.62 * eV, 12.66 * eV, 12.70 * eV,
   12.74 * eV, 12.78 * eV, 12.82 * eV, 12.86 * eV, 12.90 * eV, 12.94 * eV, 12.98 * eV, 13.02 * eV,
   13.05 * eV, 13.09 * eV, 13.13 * eV, 13.17 * eV, 13.21 * eV, 13.25 * eV, 13.29 * eV, 13.33 * eV,
   13.37 * eV, 13.41 * eV, 13.45 * eV, 13.49 * eV, 13.53 * eV, 13.57 * eV, 13.61 * eV, 13.63 * eV,
   13.64 * eV, 13.67 * eV, 13.71 * eV, 13.75 * eV, 13.79 * eV, 13.83 * eV, 13.86 * eV, 13.90 * eV,
   13.94 * eV, 13.98 * eV, 14.01 * eV, 14.05 * eV, 14.08 * eV, 14.12 * eV, 14.15 * eV, 14.18 * eV,
   14.22 * eV, 14.26 * eV, 14.27 * eV, 14.31 * eV, 14.34 * eV, 14.36 * eV, 14.39 * eV, 14.42 * eV,
   14.45 * eV, 14.49 * eV, 14.52 * eV, 14.55 * eV, 14.58 * eV, 14.61 * eV, 14.63 * eV, 14.66 * eV,
   14.69 * eV, 14.71 * eV, 14.75 * eV, 14.77 * eV, 14.79 * eV, 14.81 * eV, 14.83 * eV, 14.87 * eV,
   14.90 * eV, 14.93 * eV, 14.95 * eV, 14.98 * eV, 15.01 * eV, 15.04 * eV, 15.08 * eV, 15.10 * eV,
   15.13 * eV, 15.16 * eV, 15.18 * eV, 15.21 * eV, 15.24 * eV, 15.27 * eV, 15.30 * eV, 15.33 * eV,
   15.37 * eV, 15.40 * eV, 15.41 * eV, 15.44 * eV, 15.47 * eV, 15.50 * eV, 15.53 * eV, 15.55 * eV,
   15.56 * eV, 15.58 * eV, 15.62 * eV, 15.66 * eV, 15.69 * eV, 15.73 * eV, 15.77 * eV, 15.81 * eV,
   15.85 * eV, 15.89 * eV, 15.92 * eV, 15.96 * eV, 16.00 * eV, 16.04 * eV, 16.07 * eV, 16.11 * eV,
   16.15 * eV, 16.18 * eV, 16.21 * eV, 16.24 * eV, 16.28 * eV, 16.32 * eV, 16.36 * eV, 16.40 * eV,
   16.44 * eV, 16.48 * eV, 16.52 * eV, 16.56 * eV, 16.60 * eV, 16.64 * eV, 16.68 * eV, 16.72 * eV,
   16.74 * eV, 16.76 * eV, 16.80 * eV, 16.84 * eV, 16.88 * eV, 16.90 * eV, 16.91 * eV, 16.95 * eV,
   16.97 * eV, 17.01 * eV, 17.05 * eV, 17.08 * eV, 17.10 * eV, 17.13 * eV, 17.16 * eV, 17.20 * eV,
   17.24 * eV, 17.26 * eV, 17.28 * eV, 17.29 * eV, 17.30 * eV, 17.32 * eV, 17.35 * eV, 17.38 * eV,
   17.39 * eV, 17.40 * eV, 17.41 * eV, 17.42 * eV, 17.43 * eV, 17.45 * eV, 17.47 * eV, 17.51 * eV,
   17.54 * eV, 17.57 * eV, 17.57 * eV, 17.58 * eV, 17.60 * eV, 17.62 * eV, 17.65 * eV, 17.67 * eV,
   17.69 * eV, 17.70 * eV, 17.72 * eV, 17.73 * eV, 17.74 * eV, 17.75 * eV, 17.77 * eV, 17.79 * eV,
   17.80 * eV, 17.80 * eV, 17.82 * eV, 17.84 * eV, 17.84 * eV, 17.86 * eV, 17.88 * eV, 17.89 * eV,
   17.92 * eV, 17.93 * eV, 17.96 * eV, 17.98 * eV, 18.02 * eV, 18.04 * eV, 18.07 * eV, 18.11 * eV,
   18.14 * eV, 18.16 * eV, 18.19 * eV, 18.22 * eV, 18.23 * eV, 18.24 * eV, 18.26 * eV, 18.29 * eV,
   18.33 * eV, 18.37 * eV, 18.41 * eV, 18.45 * eV, 18.49 * eV, 18.53 * eV, 18.57 * eV, 18.61 * eV,
   18.64 * eV, 18.69 * eV, 18.73 * eV, 18.77 * eV, 18.81 * eV, 18.92 * eV, 18.96 * eV, 19.01 * eV,
   19.05 * eV, 19.09 * eV, 19.12 * eV, 19.16 * eV, 19.24 * eV, 19.28 * eV, 19.32 * eV, 19.36 * eV,
   19.38 * eV, 19.41 * eV, 19.44 * eV, 19.48 * eV, 19.51 * eV, 19.54 * eV, 19.56 * eV, 19.58 * eV,
   19.59 * eV, 19.61 * eV, 19.63 * eV, 19.65 * eV, 19.66 * eV, 19.67 * eV, 19.68 * eV, 19.71 * eV,
   19.74 * eV, 19.77 * eV, 19.77 * eV, 19.78 * eV, 19.78 * eV, 19.78 * eV, 19.79 * eV, 19.79 * eV,
   19.79 * eV, 19.80 * eV, 19.80 * eV, 19.80 * eV, 19.81 * eV, 19.82 * eV, 19.83 * eV, 19.85 * eV,
   19.88 * eV, 19.92 * eV, 19.96 * eV, 19.99 * eV
  };

  std::vector<G4double> lhe_SCINT = {0.062, 0.063, 0.065, 0.072, 0.078, 
   0.078, 0.078, 0.078, 0.076, 0.077, 0.077, 0.081, 0.085, 
   0.089, 0.088, 0.090, 0.096, 0.106, 0.104, 0.105, 0.107, 
   0.113, 0.118, 0.122, 0.125, 0.125, 0.124, 0.126, 0.133, 
   0.141, 0.144, 0.148, 0.153, 0.158, 0.156, 0.163, 0.170, 
   0.174, 0.184, 0.193, 0.207, 0.211, 0.214, 0.219, 0.229, 
   0.246, 0.255, 0.259, 0.269, 0.277, 0.284, 0.293, 0.303, 
   0.310, 0.314, 0.328, 0.337, 0.349, 0.358, 0.366, 0.378, 
   0.388, 0.395, 0.405, 0.417, 0.427, 0.439, 0.447, 0.458, 
   0.469, 0.477, 0.487, 0.498, 0.510, 0.514, 0.526, 0.536, 
   0.547, 0.558, 0.565, 0.576, 0.590, 0.602, 0.614, 0.622, 
   0.629, 0.640, 0.651, 0.659, 0.671, 0.681, 0.691, 0.702, 
   0.714, 0.724, 0.737, 0.748, 0.758, 0.768, 0.779, 0.789, 
   0.794, 0.804, 0.815, 0.827, 0.838, 0.850, 0.860, 0.870, 
   0.882, 0.891, 0.888, 0.896, 0.907, 0.916, 0.924, 0.931, 
   0.934, 0.938, 0.943, 0.956, 0.954, 0.964, 0.963, 0.954, 
   0.955, 0.967, 0.978, 0.990, 0.996, 0.996, 0.989, 0.982, 
   0.986, 0.985, 0.989, 0.989, 0.989, 0.995, 0.998, 1.000, 
   0.988, 0.978, 0.980, 0.974, 0.973, 0.959, 0.956, 0.963, 
   0.944, 0.936, 0.926, 0.920, 0.908, 0.898, 0.886, 0.892, 
   0.889, 0.879, 0.866, 0.854, 0.844, 0.835, 0.833, 0.821, 
   0.811, 0.801, 0.789, 0.778, 0.765, 0.750, 0.737, 0.725, 
   0.719, 0.704, 0.691, 0.679, 0.668, 0.656, 0.647, 0.637, 
   0.625, 0.612, 0.600, 0.586, 0.572, 0.562, 0.550, 0.540, 
   0.528, 0.516, 0.503, 0.489, 0.479, 0.469, 0.455, 0.444, 
   0.431, 0.421, 0.409, 0.401, 0.396, 0.384, 0.374, 0.364, 
   0.353, 0.345, 0.339, 0.328, 0.319, 0.308, 0.296, 0.288, 
   0.272, 0.262, 0.255, 0.254, 0.249, 0.246, 0.239, 0.231, 
   0.219, 0.208, 0.200, 0.192, 0.190, 0.197, 0.191, 0.181, 
   0.179, 0.184, 0.195, 0.203, 0.210, 0.204, 0.202, 0.213, 
   0.223, 0.232, 0.245, 0.239, 0.229, 0.238, 0.251, 0.264, 
   0.277, 0.291, 0.292, 0.281, 0.271, 0.260, 0.250, 0.249, 
   0.255, 0.241, 0.225, 0.209, 0.195, 0.183, 0.171, 0.159, 
   0.148, 0.133, 0.122, 0.108, 0.095, 0.080, 0.068, 0.058, 
   0.047, 0.037, 0.030, 0.027
  };

  // https://cdnsciencepub.com/doi/pdf/10.1139/cjr38a-014?download=true
  std::vector<G4double> lhe_RIND  (lhe_Energy.size(), 1.021);

  //basically transparent
  double absl = 100 * m;
  std::vector<G4double> lhe_ABSL  (lhe_Energy.size(), absl);


  fLHe_mt = new G4MaterialPropertiesTable();
  fLHe_mt->AddProperty("SCINTILLATIONCOMPONENT1", lhe_Energy, lhe_SCINT);
  fLHe_mt->AddProperty("SCINTILLATIONCOMPONENT2", lhe_Energy, lhe_SCINT);
  fLHe_mt->AddProperty("RINDEX", lhe_Energy, lhe_RIND);
  fLHe_mt->AddProperty("ABSLENGTH", lhe_Energy, lhe_ABSL);
  //PHYSICAL REVIEW D 105, 092005 (2022)
  fLHe_mt->AddConstProperty("SCINTILLATIONYIELD", 1250. / MeV);
  fLHe_mt->AddConstProperty("RESOLUTIONSCALE", 1.0);

  //15–25 ns from Birks, the Theory and the Practice of Scintillation Counting 
  fLHe_mt->AddConstProperty("SCINTILLATIONTIMECONSTANT1", 20. * ns);
  fLHe_mt->AddConstProperty("SCINTILLATIONYIELD1", 1.0);
  fLHe_mt->AddConstProperty("SCINTILLATIONTIMECONSTANT2", 25. * ns);
  fLHe_mt->AddConstProperty("SCINTILLATIONYIELD2", 0.0);
  fLHe->SetMaterialPropertiesTable(fLHe_mt);

  // Set the Birks Constant for the LXe scintillator
  fLHe->GetIonisation()->SetBirksConstant(0.126 * mm / MeV);

  G4MaterialPropertiesTable* vacuum_mt = new G4MaterialPropertiesTable();
  vacuum_mt->AddProperty("RINDEX", "Air");
  fVacuum->SetMaterialPropertiesTable(vacuum_mt);
  fAir->SetMaterialPropertiesTable(vacuum_mt);  // Give air the same rindex
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* LHeDetectorConstruction::Construct()
{
  // The experimental hall walls are all 1m away from housing walls
  G4double expHall_r = 3. * cm;
  G4double expHall_z = 10. * cm;

  // Create experimental hall
  fExperimentalHall_tubs =
    new G4Tubs("expHall_tubs", 0, expHall_r, expHall_z/2., 0, 360. * deg);
  fExperimentalHall_log =
    new G4LogicalVolume(fExperimentalHall_tubs, fVacuum, "expHall_log", 0, 0, 0);
  fExperimentalHall_phys = new G4PVPlacement(
    0, G4ThreeVector(), fExperimentalHall_log, "expHall", 0, false, 0);

  fExperimentalHall_log->SetVisAttributes(G4VisAttributes::GetInvisible());

  // Place the main volume
  if(fMainVolumeOn)
  {
    fMainVolume = new LHeMainVolume(0, G4ThreeVector(), fExperimentalHall_log,
                                    false, 0, this);
  }

  return fExperimentalHall_phys;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::ConstructSDandField()
{
  if(!fMainVolume)
    return;

  // Nanowire SD

  LHeNanowireSD* nanowire = fNanowire_SD.Get();
  if(!nanowire)
  {
    // Created here so it exists as nanowires are being placed
    G4cout << "Construction /LHeDet/nanowireSD" << G4endl;
    LHeNanowireSD* nanowire_SD = new LHeNanowireSD("/LHeDet/nanowireSD");
    fNanowire_SD.Put(nanowire_SD);

    nanowire_SD->InitNanowires();
    nanowire_SD->SetNanowirePositions(fMainVolume->GetNanowirePositions());
  }
  else
  {
    nanowire->InitNanowires();
    nanowire->SetNanowirePositions(fMainVolume->GetNanowirePositions());
  }
  G4SDManager::GetSDMpointer()->AddNewDetector(fNanowire_SD.Get());
  // sensitive detector is not actually on the photocathode.
  // processHits gets done manually by the stepping action.
  // It is used to detect when photons hit and get absorbed & detected at the
  // boundary to the photocathode (which doesn't get done by attaching it to a
  // logical volume.
  // It does however need to be attached to something or else it doesn't get
  // reset at the begining of events

  SetSensitiveDetector(fMainVolume->GetLogNanowire(), fNanowire_SD.Get());

  // Scint SD

  if(!fScint_SD.Get())
  {
    G4cout << "Construction /LHeDet/scintSD" << G4endl;
    LHeScintSD* scint_SD = new LHeScintSD("/LHeDet/scintSD");
    fScint_SD.Put(scint_SD);
  }
  G4SDManager::GetSDMpointer()->AddNewDetector(fScint_SD.Get());
  SetSensitiveDetector(fMainVolume->GetLogScint(), fScint_SD.Get());

  // // magnetic field ----------------------------------------------------------
  // fMagneticField = new G4UniformMagField(G4ThreeVector(0, 0, -5.*tesla));
  // fFieldMgr = new G4FieldManager();
  // fFieldMgr->SetDetectorField(fMagneticField);
  // fFieldMgr->CreateChordFinder(fMagneticField);
  // fExperimentalHall_log->SetFieldManager(fFieldMgr, forceToAllDaughters);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::SetNanowireA(G4double nanowire_a)
{
  fNanowire_a = nanowire_a;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::SetNphi(G4int nphi)
{
  fNphi = nphi;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::SetDefaults()
{
  // Resets to default values
  fD_mtl = 0.3 * mm;

  fTarget_r = 1.25 * cm;
  fTarget_z = 4 * cm;

  fScint_r = fTarget_r + 1 * cm;
  fScint_z = fTarget_z;

  fHousing_t = 2 * mm;
  fHousing_r = fScint_r + fHousing_t;
  fHousing_z = fScint_z + 2* fHousing_t;

  fNanowire_a = 60 * um;
  fNanowire_t = 10 * nm;
  fSubstrateBoard_a = 1 * cm;
  fSubstrateBoard_t = 300 * um;


  fNphi = 6;

  fOuterRadius_substrateBoard = 1 * mm;

  fRefl     = 1.0;

  fMainVolumeOn = true;
  fMainVolume   = nullptr;

  G4UImanager::GetUIpointer()->ApplyCommand(
    "/LHe/detector/scintYieldFactor 1.");

  if(fLHe_mt)
    fLHe_mt->AddConstProperty("SCINTILLATIONYIELD", 12000. / MeV);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::SetHousingReflectivity(G4double r)
{
  fRefl = r;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::SetMainVolumeOn(G4bool b)
{
  fMainVolumeOn = b;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::SetMainScintYield(G4double y)
{
  fLHe_mt->AddConstProperty("SCINTILLATIONYIELD", y / MeV);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorConstruction::SetSaveThreshold(G4int save)
{
  // Sets the save threshold for the random number seed. If the number of
  // photons generated in an event is lower than this, then save the seed for
  // this event in a file called run###evt###.rndm

  fSaveThreshold = save;
  G4RunManager::GetRunManager()->SetRandomNumberStore(true);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
