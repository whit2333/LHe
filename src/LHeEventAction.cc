//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/src/LHeEventAction.cc
/// \brief Implementation of the LHeEventAction class
//
//
#include "LHeEventAction.hh"

#include "LHeDetectorConstruction.hh"
#include "LHeHistoManager.hh"
#include "LHeNanowireHit.hh"
#include "LHeRun.hh"
#include "LHeScintHit.hh"
#include "LHeTrajectory.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4Trajectory.hh"
#include "G4TrajectoryContainer.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeEventAction::LHeEventAction(const LHeDetectorConstruction* det)
  : fDetector(det)
  , fScintCollID(-1)
  , fNanowireCollID(-1)
  , fVerbose(0)
  , fNanowireThreshold(1)
  , fForcedrawphotons(false)
  , fForcenophotons(false)
{
  fEventMessenger = new LHeEventMessenger(this);

  fHitCount                = 0;
  fPhotonCount_Scint       = 0;
  fPhotonCount_Ceren       = 0;
  fAbsorptionCount         = 0;
  fBoundaryAbsorptionCount = 0;
  fTotE                    = 0.0;

  fConvPosSet = false;
  fEdepMax    = 0.0;

  fNanowiresAboveThreshold = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeEventAction::~LHeEventAction() { delete fEventMessenger; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeEventAction::BeginOfEventAction(const G4Event*)
{
  fPrimariesPDG.clear();
  fPrimariesEnergy.clear();
  fPrimariesX.clear();
  fPrimariesY.clear();
  fPrimariesZ.clear();

  fScintHitsID.clear();
  fScintHitsX.clear();
  fScintHitsY.clear();
  fScintHitsZ.clear();
  fScintHitsEdep.clear();
  fScintHitsEdepNonIonising.clear();
  fScintHitsTOA.clear();
  fScintHitsTOAlast.clear();
  fScintHitsType.clear();

  fNanowirehitsID.clear();
  fNanowirehitsX.clear();
  fNanowirehitsY.clear();
  fNanowirehitsZ.clear();
  fNanowirehitsEdep.clear();
  fNanowirehitsEdepNonIonising.clear();
  fNanowirehitsTOA.clear();
  fNanowirehitsType.clear();

  fHitCount                = 0;
  fPhotonCount_Scint       = 0;
  fPhotonCount_Ceren       = 0;
  fAbsorptionCount         = 0;
  fBoundaryAbsorptionCount = 0;
  fTotE                    = 0.0;

  fConvPosSet = false;
  fEdepMax    = 0.0;

  fNanowiresAboveThreshold = 0;

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if(fScintCollID < 0)
    fScintCollID = SDman->GetCollectionID("scintCollection");
  if(fNanowireCollID < 0)
    fNanowireCollID = SDman->GetCollectionID("nanowireHitCollection");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeEventAction::EndOfEventAction(const G4Event* anEvent)
{

  // sanity check
  if (anEvent->GetNumberOfPrimaryVertex() == 0)
    return;

  if (anEvent->GetNumberOfPrimaryVertex() > 1)
    return;

  auto vertex = anEvent->GetPrimaryVertex(0);
  if (vertex->GetNumberOfParticle() > 1)
    return;
  auto particle = vertex->GetPrimary(0);

  G4AnalysisManager::Instance()->FillNtupleIColumn(0, anEvent->GetEventID());
  G4AnalysisManager::Instance()->FillNtupleIColumn(1, particle->GetPDGcode());
  G4AnalysisManager::Instance()->FillNtupleDColumn(2, vertex->GetX0() / CLHEP::cm);
  G4AnalysisManager::Instance()->FillNtupleDColumn(3, vertex->GetY0() / CLHEP::cm);
  G4AnalysisManager::Instance()->FillNtupleDColumn(4, vertex->GetZ0() / CLHEP::cm);
  G4AnalysisManager::Instance()->FillNtupleDColumn(5, vertex->GetT0() / CLHEP::ns);
  G4AnalysisManager::Instance()->FillNtupleDColumn(6, particle->GetTotalEnergy() / CLHEP::MeV);
  G4AnalysisManager::Instance()->FillNtupleDColumn(7, particle->GetPx() / CLHEP::MeV);
  G4AnalysisManager::Instance()->FillNtupleDColumn(8, particle->GetPy() / CLHEP::MeV);
  G4AnalysisManager::Instance()->FillNtupleDColumn(9, particle->GetPz() / CLHEP::MeV);
  G4AnalysisManager::Instance()->FillNtupleDColumn(10, particle->GetMass() / CLHEP::MeV);

  // // fill for all primary input particles
  // for (G4int iVertex = 0; iVertex < event->GetNumberOfPrimaryVertex();
  //      iVertex++) {
  //   auto vertex = event->GetPrimaryVertex(iVertex);
  //   fPrimariesX.push_back(vertex->GetX0() / CLHEP::cm);
  //   fPrimariesY.push_back(vertex->GetY0() / CLHEP::cm);
  //   fPrimariesZ.push_back(vertex->GetZ0() / CLHEP::cm);
  //   fPrimariesT.push_back(vertex->GetT0() / CLHEP::ns);
  //   for (G4int iParticle = 0; iParticle < vertex->GetNumberOfParticle();
  //        iParticle++) {
  //     auto particle = vertex->GetPrimary(iParticle);
  //     fPrimariesPDG.push_back(particle->GetPDGcode());
  //     fPrimariesEnergy.push_back(particle->GetTotalEnergy() / CLHEP::GeV);
  //   }
  // }

  G4TrajectoryContainer* trajectoryContainer =
    anEvent->GetTrajectoryContainer();

  G4int n_trajectories = 0;
  if(trajectoryContainer)
    n_trajectories = trajectoryContainer->entries();

  // extract the trajectories and draw them
  if(G4VVisManager::GetConcreteInstance())
  {
    for(G4int i = 0; i < n_trajectories; ++i)
    {
      LHeTrajectory* trj =
        (LHeTrajectory*) ((*(anEvent->GetTrajectoryContainer()))[i]);
      if(trj->GetParticleName() == "opticalphoton")
      {
        trj->SetForceDrawTrajectory(fForcedrawphotons);
        trj->SetForceNoDrawTrajectory(fForcenophotons);
      }
      trj->DrawTrajectory();
    }
  }

  LHeScintHitsCollection* scintHC = nullptr;
  LHeNanowireHitsCollection* nanowireHC     = nullptr;
  G4HCofThisEvent* hitsCE         = anEvent->GetHCofThisEvent();

  // Get the hit collections
  if(hitsCE)
  {
    if(fScintCollID >= 0)
    {
      scintHC = (LHeScintHitsCollection*) (hitsCE->GetHC(fScintCollID));
    }
    if(fNanowireCollID >= 0)
    {
      nanowireHC = (LHeNanowireHitsCollection*) (hitsCE->GetHC(fNanowireCollID));
    }
  }

  // Hits in scintillator
  if(scintHC)
  {
    size_t n_hit = scintHC->entries();
    G4ThreeVector eWeightPos(0.);
    G4double edep;
    G4double edepMax = 0;

    for(size_t i = 0; i < n_hit; ++i)
    {  // gather info on hits in scintillator
      edep = (*scintHC)[i]->GetEdep();
      fTotE += edep;
      eWeightPos +=
        (*scintHC)[i]->GetPos() * edep;  // calculate energy weighted pos
      if(edep > edepMax)
      {
        edepMax = edep;  // store max energy deposit
        G4ThreeVector posMax = (*scintHC)[i]->GetPos();
        fPosMax              = posMax;
        fEdepMax             = edep;
      }
    }

    // G4AnalysisManager::Instance()->FillH1(7, fTotE);

    if(fTotE == 0.)
    {
      if(fVerbose > 0)
        G4cout << "No hits in the scintillator this event." << G4endl;
    }
    else
    {
      // Finish calculation of energy weighted position
      eWeightPos /= fTotE;
      fEWeightPos = eWeightPos;
      if(fVerbose > 0)
      {
        G4cout << "\tEnergy weighted position of hits in LHe : "
               << eWeightPos / mm << G4endl;
      }
    }
    if(fVerbose > 0)
    {
      G4cout << "\tTotal energy deposition in scintillator : " << fTotE / keV
             << " (keV)" << G4endl;
    }
  }

  if(nanowireHC)
  {
    G4ThreeVector reconPos(0., 0., 0.);
    size_t n_hit_nanowire = nanowireHC->entries();
    // Gather info from all Nanowires
    for(size_t i = 0; i < n_hit_nanowire; ++i)
    {
      fHitCount += (*nanowireHC)[i]->GetPhotonCount();
      reconPos += (*nanowireHC)[i]->GetNanowirePos() * (*nanowireHC)[i]->GetPhotonCount();

      for (size_t j = 0; j < (*nanowireHC)[i]->GetPhotonCount(); ++ j){
        fNanowirehitsX.push_back((*nanowireHC)[i]->GetNanowireHitPos()[j][0]);
        fNanowirehitsY.push_back((*nanowireHC)[i]->GetNanowireHitPos()[j][1]);
        fNanowirehitsZ.push_back((*nanowireHC)[i]->GetNanowireHitPos()[j][2]);
        fNanowirehitsTOA.push_back((*nanowireHC)[i]->GetNanowireHitTOA()[j]);
      }
      if((*nanowireHC)[i]->GetPhotonCount() >= fNanowireThreshold)
      {
        ++fNanowiresAboveThreshold;
      }
      else
      {  // wasn't above the threshold, turn it back off
        (*nanowireHC)[i]->SetDrawit(false);
      }
    }

    // G4AnalysisManager::Instance()->FillH1(1, fHitCount);
    // G4AnalysisManager::Instance()->FillH1(2, fNanowiresAboveThreshold);

    if(fHitCount > 0)
    {  // don't bother unless there were hits
      reconPos /= fHitCount;
      if(fVerbose > 0)
      {
        G4cout << "\tReconstructed position of hits in LHe : " << reconPos / mm
               << G4endl;
      }
      fReconPos = reconPos;
    }
    nanowireHC->DrawAllHits();
  }

  // G4AnalysisManager::Instance()->FillH1(3, fPhotonCount_Scint);
  // G4AnalysisManager::Instance()->FillH1(4, fPhotonCount_Ceren);
  // G4AnalysisManager::Instance()->FillH1(5, fAbsorptionCount);
  // G4AnalysisManager::Instance()->FillH1(6, fBoundaryAbsorptionCount);
  G4AnalysisManager::Instance()->AddNtupleRow();

  if(fVerbose > 0)
  {
    // End of event output. later to be controlled by a verbose level
    G4cout << "\tNumber of photons that hit Nanowires in this event : " << fHitCount
           << G4endl;
    G4cout << "\tNumber of Nanowires above threshold(" << fNanowireThreshold
           << ") : " << fNanowiresAboveThreshold << G4endl;
    G4cout << "\tNumber of photons produced by scintillation in this event : "
           << fPhotonCount_Scint << G4endl;
    G4cout << "\tNumber of photons produced by cerenkov in this event : "
           << fPhotonCount_Ceren << G4endl;
    G4cout << "\tNumber of photons absorbed (OpAbsorption) in this event : "
           << fAbsorptionCount << G4endl;
    G4cout << "\tNumber of photons absorbed at boundaries (OpBoundary) in "
           << "this event : " << fBoundaryAbsorptionCount << G4endl;
    G4cout << "Unaccounted for photons in this event : "
           << (fPhotonCount_Scint + fPhotonCount_Ceren - fAbsorptionCount -
               fHitCount - fBoundaryAbsorptionCount)
           << G4endl;
  }

  // update the run statistics
  LHeRun* run = static_cast<LHeRun*>(
    G4RunManager::GetRunManager()->GetNonConstCurrentRun());

  run->IncHitCount(fHitCount);
  run->IncPhotonCount_Scint(fPhotonCount_Scint);
  run->IncPhotonCount_Ceren(fPhotonCount_Ceren);
  run->IncEDep(fTotE);
  run->IncAbsorption(fAbsorptionCount);
  run->IncBoundaryAbsorption(fBoundaryAbsorptionCount);
  run->IncHitsAboveThreshold(fNanowiresAboveThreshold);

  // // If we have set the flag to save 'special' events, save here
  if(fPhotonCount_Scint + fPhotonCount_Ceren < fDetector->GetSaveThreshold())
  {
    G4RunManager::GetRunManager()->SetRandomNumberStoreDir(".");
    // G4RunManager::GetRunManager()->SetRandomNumberStore(true);
    G4RunManager::GetRunManager()->rndmSaveThisEvent();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
