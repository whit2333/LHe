//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/src/LHeDetectorMessenger.cc
/// \brief Implementation of the LHeDetectorMessenger class
//
//
#include "LHeDetectorMessenger.hh"

#include "LHeDetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4Scintillation.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcommand.hh"
#include "G4UIdirectory.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeDetectorMessenger::LHeDetectorMessenger(LHeDetectorConstruction* detector)
  : fLHeDetector(detector)
{
  // Setup a command directory for detector controls with guidance
  fDetectorDir = new G4UIdirectory("/LHe/detector/");
  fDetectorDir->SetGuidance("Detector geometry control");

  fVolumesDir = new G4UIdirectory("/LHe/detector/volumes/");
  fVolumesDir->SetGuidance("Enable/disable volumes");

  // Various commands for modifying detector geometry
  fNanowireACmd = new G4UIcmdWithADoubleAndUnit("/LHe/detector/nanowire_a", this);
  fNanowireACmd->SetGuidance("Set the length scale of nanowire chip sides.");
  fNanowireACmd->SetParameterName("nanowire_a", false);
  fNanowireACmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  fNanowireACmd->SetDefaultUnit("um");
  fNanowireACmd->SetToBeBroadcasted(false);

  fNphiCmd = new G4UIcmdWithAnInteger("/LHe/detector/nphi", this);
  fNphiCmd->SetGuidance("Set the number of Nanowires along the phi-direction.");
  fNphiCmd->SetParameterName("nphi", false);
  fNphiCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  fNphiCmd->SetToBeBroadcasted(false);

  fReflectivityCmd = new G4UIcmdWithADouble("/LHe/detector/reflectivity", this);
  fReflectivityCmd->SetGuidance("Set the reflectivity of the housing.");
  fReflectivityCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  fReflectivityCmd->SetToBeBroadcasted(false);

  fLHeCmd = new G4UIcmdWithABool("/LHe/detector/volumes/LHe", this);
  fLHeCmd->SetGuidance("Enable/Disable the main detector volume.");
  fLHeCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  fLHeCmd->SetToBeBroadcasted(false);

  fMainScintYield =
    new G4UIcmdWithADouble("/LHe/detector/MainScintYield", this);
  fMainScintYield->SetGuidance("Set scinitillation yield of main volume.");
  fMainScintYield->SetGuidance("Specified in photons/MeV");
  fMainScintYield->AvailableForStates(G4State_PreInit, G4State_Idle);
  fMainScintYield->SetToBeBroadcasted(false);

  fSaveThresholdCmd = new G4UIcmdWithAnInteger("/LHe/saveThreshold", this);
  fSaveThresholdCmd->SetGuidance(
    "Set the photon count threshold for saving the random number seed");
  fSaveThresholdCmd->SetParameterName("photons", true);
  fSaveThresholdCmd->SetDefaultValue(4500);
  fSaveThresholdCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  fDefaultsCmd = new G4UIcommand("/LHe/detector/defaults", this);
  fDefaultsCmd->SetGuidance("Set all detector geometry values to defaults.");
  fDefaultsCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  fDefaultsCmd->SetToBeBroadcasted(false);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeDetectorMessenger::~LHeDetectorMessenger()
{
  delete fNanowireACmd;
  delete fNphiCmd;
  delete fLHeCmd;
  delete fReflectivityCmd;
  delete fMainScintYield;
  delete fSaveThresholdCmd;
  delete fDefaultsCmd;
  delete fDetectorDir;
  delete fVolumesDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeDetectorMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if(command == fNanowireACmd)
  {
    fLHeDetector->SetNanowireA(fNanowireACmd->GetNewDoubleValue(newValue));
  }
  else if(command == fNphiCmd)
  {
    fLHeDetector->SetNphi(fNphiCmd->GetNewIntValue(newValue));
  }
  else if(command == fReflectivityCmd)
  {
    fLHeDetector->SetHousingReflectivity(
      fReflectivityCmd->GetNewDoubleValue(newValue));
  }
  else if(command == fLHeCmd)
  {
    fLHeDetector->SetMainVolumeOn(fLHeCmd->GetNewBoolValue(newValue));
  }
  else if(command == fMainScintYield)
  {
    fLHeDetector->SetMainScintYield(
      fMainScintYield->GetNewDoubleValue(newValue));
  }
  else if(command == fSaveThresholdCmd)
  {
    fLHeDetector->SetSaveThreshold(fSaveThresholdCmd->GetNewIntValue(newValue));
  }
  else if(command == fDefaultsCmd)
  {
    fLHeDetector->SetDefaults();
    G4RunManager::GetRunManager()->ReinitializeGeometry();
  }
}
