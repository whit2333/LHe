//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/src/LHeMainVolume.cc
/// \brief Implementation of the LHeMainVolume class
//
//
#include "LHeMainVolume.hh"

#include "globals.hh"
#include "G4Box.hh"
#include "G4Polyhedra.hh"
#include "G4Colour.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4MaterialPropertiesTable.hh"
#include "G4OpticalSurface.hh"
#include "G4SystemOfUnits.hh"
#include "G4Tubs.hh"
#include "G4VisAttributes.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeMainVolume::LHeMainVolume(G4RotationMatrix* pRot, const G4ThreeVector& tlate,
                             G4LogicalVolume* pMotherLogical, G4bool pMany,
                             G4int pCopyNo, LHeDetectorConstruction* c)
  // Pass info to the G4PVPlacement constructor
  : G4PVPlacement(pRot, tlate,
                  // Temp logical volume must be created here
                  new G4LogicalVolume(new G4Box("temp", 1, 1, 1),
                                      G4Material::GetMaterial("Vacuum"), "temp",
                                      0, 0, 0),
                  "housing", pMotherLogical, pMany, pCopyNo)
  , fConstructor(c)
{
  CopyValues();

  const G4double zPlane[] = {-fScint_z/2, fScint_z/2};
  const G4double rOuter[] = {fScint_r, fScint_r}; //1/sqrt(3) = 0.577
  const G4double rInner[] = {0, 0};

  const G4double zPlane_housing[] = {-fHousing_z/2, fHousing_z/2};
  const G4double rOuter_housing[] = {fHousing_r, fHousing_r}; //1/sqrt(3) = 0.577
  const G4double rInner_housing[] = {0, 0};

  fTarget = new G4Tubs("target", 0, fTarget_r, fTarget_z/2., 0, 360. * deg);
  fScint_poly =
    new G4Polyhedra("scint_poly", 0. *deg, 360. * deg, 6, 2, zPlane, rInner, rOuter);
  fHousing_poly =
    new G4Polyhedra("housing_poly", 0. *deg, 360. * deg, 6, 2, zPlane_housing, rInner_housing, rOuter_housing);

  fHousing_log = new G4LogicalVolume(
    fHousing_poly, G4Material::GetMaterial("Al"), "housing_log", 0, 0, 0);

  fTarget_log   = new G4LogicalVolume(fTarget, G4Material::GetMaterial("ND3"),
                                   "target_log", 0, 0, 0);

  fScint_log   = new G4LogicalVolume(fScint_poly, G4Material::GetMaterial("LHe"),
                                   "scint_log", 0, 0, 0);

  // new G4PVPlacement(0, G4ThreeVector(), fTarget_log, "target",
  //                   fScint_log, false, 0);
  new G4PVPlacement(0, G4ThreeVector(), fScint_log, "scintillator",
                    fHousing_log, false, 0);

  //****************** Build Nanowires
  G4double place_r = fScint_r - fSubstrateBoard_t/2.;
  fSubstrateBoard = new G4Box("substrateBoard_tube", fSubstrateBoard_t/2., fScint_r * .577 * 2 * (fScint_r - fSubstrateBoard_t)/(fScint_r)/2., fScint_z/2.);

  // the "photocathode" is a metal slab at the back of the glass that
  // is only a very rough approximation of the real thing since it only
  // absorbs or detects the photons based on the efficiency set below
  fNanowire = new G4Box("nanowire_tube", fNanowire_t/2., fScint_r * .577 * 2 * (fScint_r - fSubstrateBoard_t)/(fScint_r)/2., fScint_z/2.);

  fSubstrateBoard_log =
    new G4LogicalVolume(fSubstrateBoard, G4Material::GetMaterial("Al"), "substrateBoard_log");
  fNanowire_log = new G4LogicalVolume(
    fNanowire, G4Material::GetMaterial("Al"), "nanowire_log");

  new G4PVPlacement(0, G4ThreeVector((fNanowire_t - fSubstrateBoard_t) / 2., 0., 0.), fNanowire_log,
                    "nanowire", fSubstrateBoard_log, false, 0);

  //***********Arrange substrateBoards around the outside of housing**********

  G4int k       = 0;

  G4double z = 0;  // front
  for (int i = 0; i < fNphi; i ++){
    theta= (30 + +360 * i / fNphi) * deg;
    z = 0;
    new G4PVPlacement(new G4RotationMatrix(G4ThreeVector(0, 0, -1), theta), G4ThreeVector(place_r*cos(theta), place_r*sin(theta), z), fSubstrateBoard_log, "substrateBoard",
                      fScint_log, false, k); ++k;
    fNanowirePositions.push_back(G4ThreeVector(place_r*cos(theta), place_r*sin(theta), z));
  }

  VisAttributes();
  SurfaceProperties();

  SetLogicalVolume(fHousing_log);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeMainVolume::CopyValues()
{
  fNphi                  = fConstructor->GetNphi();
  fTarget_r              = fConstructor->GetTargetR();
  fTarget_z              = fConstructor->GetTargetZ();
  fScint_r               = fConstructor->GetScintR();
  fScint_z               = fConstructor->GetScintZ();
  fHousing_r             = fConstructor->GetHousingR();
  fHousing_z             = fConstructor->GetHousingZ();
  fHousing_t             = fConstructor->GetHousingT();
  fNanowire_a            = fConstructor->GetNanowireA();
  fNanowire_t            = fConstructor->GetNanowireT();
  fSubstrateBoard_a      = fConstructor->GetSubstrateBoardA();
  fSubstrateBoard_t      = fConstructor->GetSubstrateBoardT();
  fRefl                  = fConstructor->GetHousingReflectivity();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeMainVolume::VisAttributes()
{
  G4VisAttributes* housing_va = new G4VisAttributes(G4Colour(1, 0, 0));
  fHousing_log->SetVisAttributes(housing_va);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeMainVolume::SurfaceProperties()
{
  //https://journals.aps.org/pra/abstract/10.1103/PhysRevA.5.372
  std::vector<G4double> ephoton = {11.90 * eV, 11.94 * eV, 11.98 * eV, 12.02 * eV, 12.06 * eV,
   12.10 * eV, 12.14 * eV, 12.18 * eV, 12.22 * eV, 12.26 * eV, 12.30 * eV, 12.34 * eV, 12.38 * eV,
   12.42 * eV, 12.46 * eV, 12.50 * eV, 12.54 * eV, 12.58 * eV, 12.62 * eV, 12.66 * eV, 12.70 * eV,
   12.74 * eV, 12.78 * eV, 12.82 * eV, 12.86 * eV, 12.90 * eV, 12.94 * eV, 12.98 * eV, 13.02 * eV,
   13.05 * eV, 13.09 * eV, 13.13 * eV, 13.17 * eV, 13.21 * eV, 13.25 * eV, 13.29 * eV, 13.33 * eV,
   13.37 * eV, 13.41 * eV, 13.45 * eV, 13.49 * eV, 13.53 * eV, 13.57 * eV, 13.61 * eV, 13.63 * eV,
   13.64 * eV, 13.67 * eV, 13.71 * eV, 13.75 * eV, 13.79 * eV, 13.83 * eV, 13.86 * eV, 13.90 * eV,
   13.94 * eV, 13.98 * eV, 14.01 * eV, 14.05 * eV, 14.08 * eV, 14.12 * eV, 14.15 * eV, 14.18 * eV,
   14.22 * eV, 14.26 * eV, 14.27 * eV, 14.31 * eV, 14.34 * eV, 14.36 * eV, 14.39 * eV, 14.42 * eV,
   14.45 * eV, 14.49 * eV, 14.52 * eV, 14.55 * eV, 14.58 * eV, 14.61 * eV, 14.63 * eV, 14.66 * eV,
   14.69 * eV, 14.71 * eV, 14.75 * eV, 14.77 * eV, 14.79 * eV, 14.81 * eV, 14.83 * eV, 14.87 * eV,
   14.90 * eV, 14.93 * eV, 14.95 * eV, 14.98 * eV, 15.01 * eV, 15.04 * eV, 15.08 * eV, 15.10 * eV,
   15.13 * eV, 15.16 * eV, 15.18 * eV, 15.21 * eV, 15.24 * eV, 15.27 * eV, 15.30 * eV, 15.33 * eV,
   15.37 * eV, 15.40 * eV, 15.41 * eV, 15.44 * eV, 15.47 * eV, 15.50 * eV, 15.53 * eV, 15.55 * eV,
   15.56 * eV, 15.58 * eV, 15.62 * eV, 15.66 * eV, 15.69 * eV, 15.73 * eV, 15.77 * eV, 15.81 * eV,
   15.85 * eV, 15.89 * eV, 15.92 * eV, 15.96 * eV, 16.00 * eV, 16.04 * eV, 16.07 * eV, 16.11 * eV,
   16.15 * eV, 16.18 * eV, 16.21 * eV, 16.24 * eV, 16.28 * eV, 16.32 * eV, 16.36 * eV, 16.40 * eV,
   16.44 * eV, 16.48 * eV, 16.52 * eV, 16.56 * eV, 16.60 * eV, 16.64 * eV, 16.68 * eV, 16.72 * eV,
   16.74 * eV, 16.76 * eV, 16.80 * eV, 16.84 * eV, 16.88 * eV, 16.90 * eV, 16.91 * eV, 16.95 * eV,
   16.97 * eV, 17.01 * eV, 17.05 * eV, 17.08 * eV, 17.10 * eV, 17.13 * eV, 17.16 * eV, 17.20 * eV,
   17.24 * eV, 17.26 * eV, 17.28 * eV, 17.29 * eV, 17.30 * eV, 17.32 * eV, 17.35 * eV, 17.38 * eV,
   17.39 * eV, 17.40 * eV, 17.41 * eV, 17.42 * eV, 17.43 * eV, 17.45 * eV, 17.47 * eV, 17.51 * eV,
   17.54 * eV, 17.57 * eV, 17.57 * eV, 17.58 * eV, 17.60 * eV, 17.62 * eV, 17.65 * eV, 17.67 * eV,
   17.69 * eV, 17.70 * eV, 17.72 * eV, 17.73 * eV, 17.74 * eV, 17.75 * eV, 17.77 * eV, 17.79 * eV,
   17.80 * eV, 17.80 * eV, 17.82 * eV, 17.84 * eV, 17.84 * eV, 17.86 * eV, 17.88 * eV, 17.89 * eV,
   17.92 * eV, 17.93 * eV, 17.96 * eV, 17.98 * eV, 18.02 * eV, 18.04 * eV, 18.07 * eV, 18.11 * eV,
   18.14 * eV, 18.16 * eV, 18.19 * eV, 18.22 * eV, 18.23 * eV, 18.24 * eV, 18.26 * eV, 18.29 * eV,
   18.33 * eV, 18.37 * eV, 18.41 * eV, 18.45 * eV, 18.49 * eV, 18.53 * eV, 18.57 * eV, 18.61 * eV,
   18.64 * eV, 18.69 * eV, 18.73 * eV, 18.77 * eV, 18.81 * eV, 18.92 * eV, 18.96 * eV, 19.01 * eV,
   19.05 * eV, 19.09 * eV, 19.12 * eV, 19.16 * eV, 19.24 * eV, 19.28 * eV, 19.32 * eV, 19.36 * eV,
   19.38 * eV, 19.41 * eV, 19.44 * eV, 19.48 * eV, 19.51 * eV, 19.54 * eV, 19.56 * eV, 19.58 * eV,
   19.59 * eV, 19.61 * eV, 19.63 * eV, 19.65 * eV, 19.66 * eV, 19.67 * eV, 19.68 * eV, 19.71 * eV,
   19.74 * eV, 19.77 * eV, 19.77 * eV, 19.78 * eV, 19.78 * eV, 19.78 * eV, 19.79 * eV, 19.79 * eV,
   19.79 * eV, 19.80 * eV, 19.80 * eV, 19.80 * eV, 19.81 * eV, 19.82 * eV, 19.83 * eV, 19.85 * eV,
   19.88 * eV, 19.92 * eV, 19.96 * eV, 19.99 * eV
  };

  //**Scintillator housing properties
  std::vector<G4double> reflectivity (ephoton.size(), fRefl);
  std::vector<G4double> efficiency   (ephoton.size(), 0);
  G4MaterialPropertiesTable* scintHsngPT = new G4MaterialPropertiesTable();
  scintHsngPT->AddProperty("REFLECTIVITY", ephoton, reflectivity);
  scintHsngPT->AddProperty("EFFICIENCY", ephoton, efficiency);
  G4OpticalSurface* OpScintHousingSurface =
    new G4OpticalSurface("HousingSurface", unified, polished, dielectric_metal);
  OpScintHousingSurface->SetMaterialPropertiesTable(scintHsngPT);

  //**NbN thin film surface properties
  //https://opg.optica.org/ome/fulltext.cfm?uri=ome-8-8-2072&id=395101
  std::vector<G4double> nanowire_EFF     (ephoton.size(), 1);
  std::vector<G4double> nanowire_ReR     (ephoton.size(), 2.);
  std::vector<G4double> nanowire_ImR     (ephoton.size(), 1.5);
  G4MaterialPropertiesTable* nanowire_mt = new G4MaterialPropertiesTable();
  nanowire_mt->AddProperty("EFFICIENCY", ephoton, nanowire_EFF);
  nanowire_mt->AddProperty("REALRINDEX", ephoton, nanowire_ReR);
  nanowire_mt->AddProperty("IMAGINARYRINDEX", ephoton, nanowire_ImR);
  G4OpticalSurface* nanowire_opsurf = new G4OpticalSurface(
    "nanowire_opsurf", glisur, polished, dielectric_metal);
  nanowire_opsurf->SetMaterialPropertiesTable(nanowire_mt);

  //**Create logical skin surfaces
  new G4LogicalSkinSurface("nanowire_surf", fNanowire_log, nanowire_opsurf);
}
