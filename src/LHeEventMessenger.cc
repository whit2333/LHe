//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/src/LHeEventMessenger.cc
/// \brief Implementation of the LHeEventMessenger class
//
//
#include "LHeEventMessenger.hh"

#include "LHeEventAction.hh"

#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeEventMessenger::LHeEventMessenger(LHeEventAction* event)
  : fLHeEvent(event)
{
  fVerboseCmd = new G4UIcmdWithAnInteger("/LHe/eventVerbose", this);
  fVerboseCmd->SetGuidance("Set the verbosity of event data.");
  fVerboseCmd->SetParameterName("verbose", true);
  fVerboseCmd->SetDefaultValue(1);

  fSubstrateBoardThresholdCmd = new G4UIcmdWithAnInteger("/LHe/substrateBoardThreshold", this);
  fSubstrateBoardThresholdCmd->SetGuidance("Set the substrateBoardThreshold (in # of photons)");

  fForceDrawPhotonsCmd = new G4UIcmdWithABool("/LHe/forceDrawPhotons", this);
  fForceDrawPhotonsCmd->SetGuidance("Force drawing of photons.");
  fForceDrawPhotonsCmd->SetGuidance(
    "(Higher priority than /LHe/forceDrawNoPhotons)");

  fForceDrawNoPhotonsCmd =
    new G4UIcmdWithABool("/LHe/forceDrawNoPhotons", this);
  fForceDrawNoPhotonsCmd->SetGuidance("Force no drawing of photons.");
  fForceDrawNoPhotonsCmd->SetGuidance(
    "(Lower priority than /LHe/forceDrawPhotons)");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeEventMessenger::~LHeEventMessenger()
{
  delete fVerboseCmd;
  delete fSubstrateBoardThresholdCmd;
  delete fForceDrawPhotonsCmd;
  delete fForceDrawNoPhotonsCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeEventMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if(command == fVerboseCmd)
  {
    fLHeEvent->SetEventVerbose(fVerboseCmd->GetNewIntValue(newValue));
  }
  else if(command == fSubstrateBoardThresholdCmd)
  {
    fLHeEvent->SetNanowireThreshold(fSubstrateBoardThresholdCmd->GetNewIntValue(newValue));
  }
  else if(command == fForceDrawPhotonsCmd)
  {
    fLHeEvent->SetForceDrawPhotons(
      fForceDrawPhotonsCmd->GetNewBoolValue(newValue));
  }
  else if(command == fForceDrawNoPhotonsCmd)
  {
    fLHeEvent->SetForceDrawNoPhotons(
      fForceDrawNoPhotonsCmd->GetNewBoolValue(newValue));
  }
}
