//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/src/LHeRunAction.cc
/// \brief Implementation of the LHeRunAction class
//
//
#include "LHeRunAction.hh"
#include "LHeEventAction.hh"

#include "G4UserRunAction.hh"
#include "LHeHistoManager.hh"
#include "LHeRun.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeRunAction::LHeRunAction(LHeEventAction *eventAction)
  :  G4UserRunAction()
  ,  fRun(nullptr)
  , fHistoManager(nullptr)
  , fEventAction(eventAction)
{
  // Book predefined histograms
  fHistoManager = new LHeHistoManager();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

LHeRunAction::~LHeRunAction() { delete fHistoManager; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4Run* LHeRunAction::GenerateRun()
{
  fRun = new LHeRun();
  return fRun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeRunAction::BeginOfRunAction(const G4Run*)
{
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  G4String fileName = "LHe.root";
  // analysisManager -> SetActivation(true);
  // if(analysisManager->IsActive())
  // {
  if (fEventAction){
    analysisManager->CreateNtuple("hits", "hits");
    analysisManager->CreateNtupleIColumn("eventID");
    analysisManager->CreateNtupleIColumn("primaryPID");
    analysisManager->CreateNtupleDColumn("primaryX");
    analysisManager->CreateNtupleDColumn("primaryY");
    analysisManager->CreateNtupleDColumn("primaryZ");
    analysisManager->CreateNtupleDColumn("primaryT");
    analysisManager->CreateNtupleDColumn("primaryE");
    analysisManager->CreateNtupleDColumn("primaryPx");
    analysisManager->CreateNtupleDColumn("primaryPy");
    analysisManager->CreateNtupleDColumn("primaryPz");
    analysisManager->CreateNtupleDColumn("primaryM");


    analysisManager->CreateNtupleDColumn("hits_x",
                                         fEventAction->fNanowirehitsX);
    analysisManager->CreateNtupleDColumn("hits_y",
                                         fEventAction->fNanowirehitsY);
    analysisManager->CreateNtupleDColumn("hits_z",
                                         fEventAction->fNanowirehitsZ);
    analysisManager->CreateNtupleDColumn("hits_t",
                                         fEventAction->fNanowirehitsTOA);
    analysisManager->FinishNtuple();
  }
  analysisManager->OpenFile();
  // }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void LHeRunAction::EndOfRunAction(const G4Run*)
{
  if(isMaster)
    fRun->EndOfRun();

  // save histograms
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  // if(analysisManager->IsActive())
  // {
    analysisManager->Write();
    analysisManager->CloseFile();
  // }
}
