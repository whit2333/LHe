//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/include/LHeScintHit.hh
/// \brief Definition of the LHeScintHit class
//
//
#ifndef LHeScintHit_h
#define LHeScintHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4VPhysicalVolume.hh"

class LHeScintHit : public G4VHit
{
 public:
  LHeScintHit();
  LHeScintHit(G4VPhysicalVolume* pVol);
  ~LHeScintHit();

  LHeScintHit(const LHeScintHit& right);
  const LHeScintHit& operator=(const LHeScintHit& right);
  G4bool operator==(const LHeScintHit& right) const;

  inline void* operator new(size_t);
  inline void operator delete(void* aHit);

  inline void SetEdep(G4double de) { fEdep = de; }
  inline void AddEdep(G4double de) { fEdep += de; }
  inline G4double GetEdep() { return fEdep; }
  inline void SetTOA(G4double time) { fTimeOfArrival = time; }
  inline G4double GetTOA() const { return fTimeOfArrival; }

  inline void SetPos(G4ThreeVector xyz) { fPos = xyz; }
  inline G4ThreeVector GetPos() { return fPos; }

  inline const G4VPhysicalVolume* GetPhysV() { return fPhysVol; }

 private:
  G4double fEdep;
  G4double fTimeOfArrival;
  G4ThreeVector fPos;
  const G4VPhysicalVolume* fPhysVol;
};

typedef G4THitsCollection<LHeScintHit> LHeScintHitsCollection;

extern G4ThreadLocal G4Allocator<LHeScintHit>* LHeScintHitAllocator;

inline void* LHeScintHit::operator new(size_t)
{
  if(!LHeScintHitAllocator)
    LHeScintHitAllocator = new G4Allocator<LHeScintHit>;
  return (void*) LHeScintHitAllocator->MallocSingle();
}

inline void LHeScintHit::operator delete(void* aHit)
{
  LHeScintHitAllocator->FreeSingle((LHeScintHit*) aHit);
}

#endif
