//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/include/LHeSteppingAction.hh
/// \brief Definition of the LHeSteppingAction class
//
#ifndef LHeSteppingAction_h
#define LHeSteppingACtion_h 1

#include "globals.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4UserSteppingAction.hh"

class LHeEventAction;
class LHeTrackingAction;
class LHeSteppingMessenger;

class LHeSteppingAction : public G4UserSteppingAction
{
 public:
  LHeSteppingAction(LHeEventAction*);
  ~LHeSteppingAction();

  void UserSteppingAction(const G4Step*) override;

  void SetOneStepPrimaries(G4bool b) { fOneStepPrimaries = b; }
  G4bool GetOneStepPrimaries() { return fOneStepPrimaries; }

 private:
  G4bool fOneStepPrimaries;
  LHeSteppingMessenger* fSteppingMessenger;
  LHeEventAction* fEventAction;
  G4OpBoundaryProcess* fBoundary = nullptr;

  G4OpBoundaryProcessStatus fExpectedNextStatus;
};

#endif
