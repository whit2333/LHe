//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/include/LHeEventAction.hh
/// \brief Definition of the LHeEventAction class
//

#ifndef LHeEventAction_h
#define LHeEventAction_h 1

#include "LHeEventMessenger.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4UserEventAction.hh"

class G4Event;
class LHeDetectorConstruction;

class LHeEventAction : public G4UserEventAction
{
 public:
  LHeEventAction(const LHeDetectorConstruction*);
  ~LHeEventAction();

 public:
  void BeginOfEventAction(const G4Event*) override;
  void EndOfEventAction(const G4Event*) override;

  void SetEventVerbose(G4int v) { fVerbose = v; }

  void SetNanowireThreshold(G4int t) { fNanowireThreshold = t; }

  void SetForceDrawPhotons(G4bool b) { fForcedrawphotons = b; }
  void SetForceDrawNoPhotons(G4bool b) { fForcenophotons = b; }

  void IncPhotonCount_Scint() { ++fPhotonCount_Scint; }
  void IncPhotonCount_Ceren() { ++fPhotonCount_Ceren; }
  void IncEDep(G4double dep) { fTotE += dep; }
  void IncAbsorption() { ++fAbsorptionCount; }
  void IncBoundaryAbsorption() { ++fBoundaryAbsorptionCount; }
  void IncHitCount(G4int i = 1) { fHitCount += i; }

  void SetEWeightPos(const G4ThreeVector& p) { fEWeightPos = p; }
  void SetReconPos(const G4ThreeVector& p) { fReconPos = p; }
  void SetConvPos(const G4ThreeVector& p)
  {
    fConvPos    = p;
    fConvPosSet = true;
  }
  void SetPosMax(const G4ThreeVector& p, G4double edep)
  {
    fPosMax  = p;
    fEdepMax = edep;
  }

  G4int GetPhotonCount_Scint() const { return fPhotonCount_Scint; }
  G4int GetPhotonCount_Ceren() const { return fPhotonCount_Ceren; }
  G4int GetHitCount() const { return fHitCount; }
  G4double GetEDep() const { return fTotE; }
  G4int GetAbsorptionCount() const { return fAbsorptionCount; }
  G4int GetBoundaryAbsorptionCount() const { return fBoundaryAbsorptionCount; }

  G4ThreeVector GetEWeightPos() { return fEWeightPos; }
  G4ThreeVector GetReconPos() { return fReconPos; }
  G4ThreeVector GetConvPos() { return fConvPos; }
  G4ThreeVector GetPosMax() { return fPosMax; }
  G4double GetEDepMax() { return fEdepMax; }
  G4double IsConvPosSet() { return fConvPosSet; }

  // Gets the total photon count produced
  G4int GetPhotonCount() { return fPhotonCount_Scint + fPhotonCount_Ceren; }

  void IncNanowireSAboveThreshold() { ++fNanowiresAboveThreshold; }
  G4int GetNanowireSAboveThreshold() { return fNanowiresAboveThreshold; }

  /// Vector of primary particles in an event: PDG type
  std::vector<G4int> fPrimariesPDG;
  /// Vector of primary particles in an event: particle energy (in GeV)
  std::vector<G4double> fPrimariesEnergy;
  /// Vector of primary particles in an event: vertex position x (in cm)
  std::vector<G4double> fPrimariesX;
  /// Vector of primary particles in an event: vertex position y (in cm)
  std::vector<G4double> fPrimariesY;
  /// Vector of primary particles in an event: vertex position z (in cm)
  std::vector<G4double> fPrimariesZ;

  /// Vector of hits in scintillators: hit ID
  std::vector<G4int> fScintHitsID;
  /// Vector of hits in scintillators: hit position x (in cm)
  std::vector<G4double> fScintHitsX;
  /// Vector of hits in scintillators: hit position y (in cm)
  std::vector<G4double> fScintHitsY;
  /// Vector of hits in scintillators: hit position z (in cm)
  std::vector<G4double> fScintHitsZ;
  /// Vector of hits in scintillators: hit energy (in keV)
  std::vector<G4double> fScintHitsEdep;
  /// Vector of hits in scintillators: hit non-ionizing energy (in keV)
  std::vector<G4double> fScintHitsEdepNonIonising;
  /// Vector of hits in scintillators: hit time of arrival (in ns)
  /// calculated as global time of energy deposit which added to hit energy
  /// exceeds the toa threshold (by default threshold is 0, so it is the first
  /// deposit)
  std::vector<G4double> fScintHitsTOA;
  /// Vector of hits in scintillators: hit time of last arrival (in ns)
  /// calculated as global time of energy deposit which is the last deposit
  /// that fits within the digitisation time window (by default window is
  /// undefined, so it is the last deposit)
  std::vector<G4double> fScintHitsTOAlast;
  /// Vector of hits in scintillators: hit type
  /// Simulation defines only hits of type 0 (hexagonal cells, no calibration or
  /// edge cell is constructed)
  std::vector<G4int> fScintHitsType;

  /// Vector of hits in Nanowire: hit ID
  std::vector<G4int> fNanowirehitsID;
  /// Vector of hits in Nanowire: hit position x (in cm)
  std::vector<G4double> fNanowirehitsX;
  /// Vector of hits in Nanowire: hit position y (in cm)
  std::vector<G4double> fNanowirehitsY;
  /// Vector of hits in Nanowire: hit position z (in cm)
  std::vector<G4double> fNanowirehitsZ;
  /// Vector of hits in Nanowire: hit energy (in keV)
  std::vector<G4double> fNanowirehitsEdep;
  /// Vector of hits in Nanowire: hit non-ionizing energy (in keV)
  std::vector<G4double> fNanowirehitsEdepNonIonising;
  /// Vector of hits in Nanowire: hit time of last arrival (in ns)
  /// calculated as global time of energy deposit which is the last deposit
  /// that fits within the digitisation time window (by default window is
  /// undefined, so it is the last deposit)
  std::vector<G4double> fNanowirehitsTOA;
  /// Vector of hits in Nanowire sensors: hit type
  /// Simulation defines only hits of type 1
  std::vector<G4int> fNanowirehitsType;


 private:
  LHeEventMessenger* fEventMessenger;
  const LHeDetectorConstruction* fDetector;

  G4int fScintCollID;
  G4int fNanowireCollID;

  G4int fVerbose;

  G4int fNanowireThreshold;

  G4bool fForcedrawphotons;
  G4bool fForcenophotons;

  G4int fHitCount;
  G4int fPhotonCount_Scint;
  G4int fPhotonCount_Ceren;
  G4int fAbsorptionCount;
  G4int fBoundaryAbsorptionCount;

  G4double fTotE;

  // These only have meaning if totE > 0
  // If totE = 0 then these won't be set by EndOfEventAction
  G4ThreeVector fEWeightPos;
  G4ThreeVector fReconPos;  // Also relies on hitCount>0
  G4ThreeVector fConvPos;   // true (initial) converstion position
  G4bool fConvPosSet;
  G4ThreeVector fPosMax;
  G4double fEdepMax;

  G4int fNanowiresAboveThreshold;
};

#endif
