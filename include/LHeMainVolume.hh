//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/include/LHeMainVolume.hh
/// \brief Definition of the LHeMainVolume class
//
#ifndef LHeMainVolume_h
#define LHeMainVolume_h 1

#include "LHeDetectorConstruction.hh"

#include "G4PVPlacement.hh"
#include "math.h"

class G4Box;
class G4Polyhedra;
class G4LogicalVolume;
class G4Tubs;

class LHeMainVolume : public G4PVPlacement
{
 public:
  LHeMainVolume(G4RotationMatrix* pRot, const G4ThreeVector& tlate,
                G4LogicalVolume* pMotherLogical, G4bool pMany, G4int pCopyNo,
                LHeDetectorConstruction* c);

  G4LogicalVolume* GetLogNanowire() { return fNanowire_log; }
  G4LogicalVolume* GetLogScint() { return fScint_log; }

  std::vector<G4ThreeVector> GetNanowirePositions() { return fNanowirePositions; }

 private:
  void VisAttributes();
  void SurfaceProperties();
  void CopyValues();

  LHeDetectorConstruction* fConstructor;

  G4double fTarget_r;
  G4double fTarget_z;
  G4double fScint_r;
  G4double fScint_z;
  G4double fHousing_r;
  G4double fHousing_z;
  G4double fHousing_t;
  G4double fNanowire_a;
  G4double fNanowire_t;
  G4double fSubstrateBoard_a;
  G4double fSubstrateBoard_t;
  G4double theta;

  G4int fNx;
  G4int fNy;
  G4int fNphi;
  G4int fNz;
  G4double fOuterRadius_substrateBoard;
  G4double fRefl;

  // Basic Volumes
  //
  G4Box* fScint_box;
  G4Box* fHousing_box;
  G4Polyhedra* fScint_poly;
  G4Polyhedra* fHousing_poly;
  G4Box* fSubstrateBoard;
  G4Box* fNanowire;

  G4Tubs* fTarget;

  // Logical volumes
  //
  G4LogicalVolume* fTarget_log;
  G4LogicalVolume* fScint_log;
  G4LogicalVolume* fHousing_log;
  G4LogicalVolume* fSubstrateBoard_log;
  G4LogicalVolume* fNanowire_log;

  // Sensitive Detectors positions
  std::vector<G4ThreeVector> fNanowirePositions;
};

#endif
