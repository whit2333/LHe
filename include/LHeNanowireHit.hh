//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/include/LHeNanowireHit.hh
/// \brief Definition of the LHeNanowireHit class
//
//
#ifndef LHeNanowireHit_h
#define LHeNanowireHit_h 1

#include "G4Allocator.hh"
#include "G4LogicalVolume.hh"
#include "G4THitsCollection.hh"
#include "G4VHit.hh"
#include "G4VPhysicalVolume.hh"

class LHeNanowireHit : public G4VHit
{
 public:
  LHeNanowireHit();
  LHeNanowireHit(const LHeNanowireHit& right);
  ~LHeNanowireHit();

  const LHeNanowireHit& operator=(const LHeNanowireHit& right);
  G4bool operator==(const LHeNanowireHit& right) const;

  inline void* operator new(size_t);
  inline void operator delete(void* aHit);

  virtual void Draw();
  virtual void Print();

  inline void SetDrawit(G4bool b) { fDrawit = b; }
  inline G4bool GetDrawit() { return fDrawit; }

  inline void IncPhotonCount() { ++fPhotons; }
  inline G4int GetPhotonCount() { return fPhotons; }

  inline void SetNanowireNumber(G4int n) { fNanowireNumber = n; }
  inline G4int GetNanowireNumber() { return fNanowireNumber; }

  inline void SetNanowirePhysVol(G4VPhysicalVolume* physVol)
  {
    this->fPhysVol = physVol;
  }
  inline G4VPhysicalVolume* GetNanowirePhysVol() { return fPhysVol; }

  inline void SetNanowirePos(G4double x, G4double y, G4double z)
  {
    fPos = G4ThreeVector(x, y, z);
  }

  inline void SetNanowireHitTOA(G4double time) { fHitTimeOfArrival.push_back(time); }
  inline void SetNanowireHitPos(G4ThreeVector pos) { fHitPos.push_back(pos); }

  inline G4ThreeVector GetNanowirePos() { return fPos; }
  inline std::vector<G4ThreeVector> GetNanowireHitPos() const { return fHitPos; }
  inline std::vector<G4double> GetNanowireHitTOA() const { return fHitTimeOfArrival; }

 private:
  G4int fNanowireNumber;
  G4int fPhotons;
  G4ThreeVector fPos;
  std::vector<G4ThreeVector> fHitPos;
  std::vector<G4double> fHitTimeOfArrival;
  G4VPhysicalVolume* fPhysVol;
  G4bool fDrawit;
};

typedef G4THitsCollection<LHeNanowireHit> LHeNanowireHitsCollection;

extern G4ThreadLocal G4Allocator<LHeNanowireHit>* LHeNanowireHitAllocator;

inline void* LHeNanowireHit::operator new(size_t)
{
  if(!LHeNanowireHitAllocator)
    LHeNanowireHitAllocator = new G4Allocator<LHeNanowireHit>;
  return (void*) LHeNanowireHitAllocator->MallocSingle();
}

inline void LHeNanowireHit::operator delete(void* aHit)
{
  LHeNanowireHitAllocator->FreeSingle((LHeNanowireHit*) aHit);
}

#endif
