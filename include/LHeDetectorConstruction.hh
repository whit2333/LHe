//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file optical/LHe/include/LHeDetectorConstruction.hh
/// \brief Definition of the LHeDetectorConstruction class
//
//
#ifndef LHeDetectorConstruction_h
#define LHeDetectorConstruction_h 1

#include "LHeDetectorMessenger.hh"

#include "G4Cache.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"

class LHeMainVolume;
class LHeNanowireSD;
class LHeScintSD;

class G4Box;
class G4Element;
class G4LogicalVolume;
class G4Material;
class G4MaterialPropertiesTable;
class G4Tubs;
class G4VPhysicalVolume;

class LHeDetectorConstruction : public G4VUserDetectorConstruction
{
 public:
  LHeDetectorConstruction();
  ~LHeDetectorConstruction();

  G4VPhysicalVolume* Construct() override;
  void ConstructSDandField() override;

  // Functions to modify the geometry
  void SetTargetR(G4double);
  void SetTargetZ(G4double);
  void SetScintR(G4double);
  void SetScintZ(G4double);
  void SetHousingR(G4double);
  void SetHousingZ(G4double);
  void SetHousingT(G4double);
  void SetNanowireA(G4double);
  void SetNanowireT(G4double);
  void SetSubstrateBoardA(G4double);
  void SetSubstrateBoardT(G4double);
  void SetNphi(G4int);

  void SetDefaults();
  void SetSaveThreshold(G4int);

  // Get values
  G4int GetNphi() const { return fNphi; };
  G4int GetSaveThreshold() const { return fSaveThreshold; };

  G4double GetTargetR() const { return fTarget_r; }
  G4double GetTargetZ() const { return fTarget_z; }
  G4double GetScintR() const { return fScint_r; }
  G4double GetScintZ() const { return fScint_z; }
  G4double GetHousingR() const { return fHousing_r; }
  G4double GetHousingZ() const { return fHousing_z; }
  G4double GetHousingT() const { return fHousing_t; }
  G4double GetNanowireA() const { return fNanowire_a; }
  G4double GetNanowireT() const { return fNanowire_t; }
  G4double GetSubstrateBoardA() const { return fSubstrateBoard_a; }
  G4double GetSubstrateBoardT() const { return fSubstrateBoard_t; }

  void SetHousingReflectivity(G4double);
  G4double GetHousingReflectivity() const { return fRefl; }

  void SetMainVolumeOn(G4bool b);
  G4bool GetMainVolumeOn() const { return fMainVolumeOn; }

  void SetMainScintYield(G4double);

 private:
  void DefineMaterials();

  LHeDetectorMessenger* fDetectorMessenger;

  G4Tubs* fExperimentalHall_tubs;
  G4LogicalVolume* fExperimentalHall_log;
  G4VPhysicalVolume* fExperimentalHall_phys;

  // Materials & Elements
  G4Material* fLHe;
  G4Material* fAl;
  G4Element* fN;
  G4Element* fO;
  G4Material* fAir;
  G4Material* fND3;
  G4Material* fVacuum;
  G4Element* fC;
  G4Element* fH;
  G4Element* fHe;
  G4Element* fD;

  // Geometry
  G4double fD_mtl;
  G4int fNphi;
  // // Geometry
  G4double fTarget_r;
  G4double fTarget_z;
  G4double fScint_r;
  G4double fScint_z;
  G4double fHousing_r;
  G4double fHousing_z;
  G4double fHousing_t;
  G4double fNanowire_a;
  G4double fNanowire_t;
  G4double fSubstrateBoard_a;
  G4double fSubstrateBoard_t;

  G4int fSaveThreshold;
  G4double fOuterRadius_substrateBoard;
  G4double fRefl;

  G4bool fMainVolumeOn;

  LHeMainVolume* fMainVolume;

  G4MaterialPropertiesTable* fLHe_mt;

  // Sensitive Detectors
  G4Cache<LHeScintSD*> fScint_SD;
  G4Cache<LHeNanowireSD*> fNanowire_SD;

  G4UniformMagField* fMagneticField;
  G4FieldManager* fFieldMgr;
  G4bool forceToAllDaughters = true;

};

#endif
